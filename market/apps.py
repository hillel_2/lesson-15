from django.apps import AppConfig
from django.template.defaultfilters import register


class MarketConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'market'
    verbose_name = "Market Market"


@register.filter
def ao(value, only_capital=True):
    value = str(value)
    if only_capital:
        return value.replace('A', 'O')
    return value.replace('a', 'o').replace('A', 'O')
